CC=gcc
CFLAGS=-c -Wall -I./include
SOURCE=./src/main.c ./src/nodo.c ./src/reader.c ./src/user.c ./src/flib.c
OBJECT=$(SOURCE:.c=.o)

all: info 

info: $(OBJECT)
	$(CC) -o info $(OBJECT) -I.
	
main.o: main.c
	$(CC) $(CFLAGS) main.c

reader.o: reader.c
	$(CC) $(CFLAGS) reader.c

nodo.o: nodo.c
	$(CC) $(CFLAGS) nodo.c
	
user.o: user.c
	$(CC) $(CFLAGS) user.c

flib.o: flib.c
	$(CC) $(CFLAGS) flib.c

.PHONY: clean
clean:
	rm -rf ./src/*.o info
