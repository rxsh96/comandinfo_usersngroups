# Comando Información Usuarios y Grupos #

Proyecto Primer Parcial de la materia Programación de Sistemas (CCPG1008) Paralelo 1 de la ESPOL.

### Descripción ###

* El programa acepta un argumento que lo interpreta como el usuario a mostrar.
* Si el argumento usa la opción -g, este se interpreta como un grupo y se listan todos los usuarios que pertenecen al mismo.
* El programa lee el archivo /etc/passwd creando una estructura Usuario por cada línea del archivo.
* Cada usuario se agrega a una lista enlazada.

### Funcionamiento ###
A continuación se muestra la salida esperada: 

* Si ejecutamos $info usuario: 

$ info rxsh96  
Información de rxsh96  
UID: 1000  
Descripción: Ricardo Serrano  
Home: /home/rxsh96  
Shell: /bin/bash  

* Si ejecutamos $info -g grupo: 

$info –g adm  
Miembros de adm:  

Usuario: jbeltran1997  
Descripción: Johnny Beltran  

Usuario: rxsh96  
Descripción: Ricardo Serrano  

### Integrantes ###

* Johnny Beltran
* Ricardo Serrano
