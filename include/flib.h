/**
 * flib.h
 * @author Ricardo Serrano
 * @author Johnny Beltran
 */

/**
 *Includes 
 */
#include <getopt.h>
#include <user.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

/** 
 * Definicion de las rutas absolutas de los archivos
 */
#define USERFILENAME "/etc/passwd"
#define GROUPFILENAME "/etc/group"

/**
 * Prototipo de los metodos
 */
bool validateFile(char *);
void showHelp();
void validateUser(User);
char *strlwr(char *);
int descripcionLen(char *);
bool compareStr(char *nombre, char *descripcion);
void saveDescriptionProperty(User *, char *);
void trim(char *);
