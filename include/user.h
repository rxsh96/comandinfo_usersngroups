/**
 * user.h
 * @author Ricardo Serrano
 * @author Johnny Beltran
 */

#ifndef _USER_H_
#define _USER_H_

/**
 *Includes 
 */
#include <stdio.h> 

/**
 *Definicion de la estructura User para el usuario
 */
typedef struct User{
	char *id;
	char *username; 
	char *description;
	char *home;
	char *shell;
} User;

/**
 * Prototipo de los metodos
 */
void toString(User);
User nullUser();

#endif 
