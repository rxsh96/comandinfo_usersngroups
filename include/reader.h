/**
 * reader.h
 * @author Ricardo Serrano
 * @author Johnny Beltran
 */

#ifndef _READER_H_
#define _READER_H_

/**
 *Includes 
 */
#include <stdio.h>
#include <nodo.h>
#include <user.h>

/**
 * Prototipo de los metodos
 */
bool loadUsers(char *, Node **);
bool displayGroupUsers(char *, char *, Node *);
void tokenizer(char *, User *);

#endif 
