/**
 * nodo.h
 * @author Ricardo Serrano
 * @author Johnny Beltran
 */

#ifndef _NODO_H_
#define _NODO_H_

/**
 *Includes 
 */
#include <stdbool.h>
#include <user.h>


/**
 *Definicion de la estructura Node para los nodos
 */
typedef struct Node{
	User user;
	struct Node *next;
} Node;

/**
 * Prototipo de los metodos
 */
bool isEmpty(Node *);
Node *getNext(Node *);
Node *addNode(User *, Node *);
void printList(Node *);
User getNode(char *, Node *);

#endif 
