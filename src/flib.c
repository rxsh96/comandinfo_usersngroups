/**
 * flib.c
 * Implementacion de metodos adicionales para el correcto funcionamiento del programa.
 * @author Ricardo Serrano
 * @author Johnny Beltran
 */

/**
 *Includes 
 */
#include <flib.h>

/**
 * Metodo que valida el acceso a un archivo
 * @param filename, ruta absoluta del archivo
 * @return true si el archivo fue encontrado, false si el archivo no fue encontrado
 */
bool validateFile(char *fileName){
    FILE *file;
    if ((file = fopen(fileName, "r")) != NULL){
        fclose(file);
        return true;
    }
    return false;
}

/** 
 * Muestra informacion general sobre como usar el progama.
 */
void showHelp(){
	printf("Usage: ./info [OPTION] USER|GROUP\n");
	printf("Muestra la informacion de USER almacenada en /etc/passwd\n");
	printf("-h: Ayuda, muestra este mensaje\n");
	printf("-g GRUPO: Muestra la información de todos los usuarios en GRUPO\n");
}

/** 
 * Valida que el nombre de usuario exista. 
 * @param user el usuario
 */ 
void validateUser(User user){
	if(strcmp(user.description,"") == 0){
		fprintf(stderr, "El usuario ingresado no existe!\n");
	}
	else{
		printf("Informacion de %s\n", user.username);
		toString(user);
	}
}

/**
 * Transforma una cadena con mayusculas a minusculas
 * @param string, la cadena
 * @return cadena en minuscula
 */
char *strlwr(char *string){
	char *lowerString = (char *) malloc(strlen(string)*sizeof(char *));
	for(int i = 0; i < strlen(string); i++){
		lowerString[i] = tolower(string[i]);
	}
	return lowerString;
}

/**
 * Guarda la descripcion del Usuario correctamente
 * @param user, el usuario
 * @param token, la informacion del usuario a guardar
 */
void saveDescriptionProperty(User *user, char *token){
	if(strchr(token, ',')){
		memmove(user->description, token, strlen(token)-3);
	}
	else{
		if(strcmp(token, "") == 0){
			strcpy(user->description, "Sin descripcion");
		}
		else{
			strcpy(user->description, token);
		}
	}
}

/**
 * Elimina el salto de linea de una cadena
 * @param string, la cadena
 */
void trim(char *string){
	int last = strlen(string)-1;
	if(string[last]=='\n'){
		string[last] = 0;
	}
}

/* METODOS PARA COMPARAR USERNAME CON DESCRIPCION
int descripcionLen(char *description){
	if(strchr(description, ' ')){
		char *final = strchr(description, ' ');
		return (int)(final - description);
	}
	return (int)strlen(description);
}

bool compareStr(char *nombre, char *description){
	for(int i = 0 ; i < descripcionLen(description) ; i++){
		if(tolower(nombre[i]) != tolower(description[i])){
			return false;
		}
	}
	return true;
}
*/



