/**
 * nodo.c
 * Implementacion de los metodos para la estructura Node.
 * @author Ricardo Serrano
 * @author Johnny Beltran
 */

/**
 *Includes 
 */
#include <nodo.h>
#include <stdlib.h>
#include <string.h>
#include <flib.h>

/**
 * Verifica que el nodo no sea nulo
 * @param nodo, el nodo que contiene la informacion
 * @return true si el nodo es nulo, false si no es nulo
 */
bool isEmpty(Node *nodo){
	return (nodo == NULL);
}

/**
 * Obtiene el nodo siguiente al nodo especificado
 * @param nodo, el nodo
 * @return el nodo siguiente 
 */
Node *getNext(Node *nodo){
	if (!isEmpty(nodo)){
		return nodo->next;
	}
	return NULL;
}

/**
 * Agrega el usuario al nodo y el nodo a una lista enlazada
 * @param user, el usuario a guardar en el nodo
 * @param next, referencia al nodo siguiente del nodo a crear
 * @return nodo, contiene al usuario
 */
Node *addNode(User *user, Node *next){
	Node *nodo;
	nodo = (Node *) malloc(sizeof(Node));

	nodo->user = *user;
	nodo->next = next;
	return nodo;
}

/**
 * Imprema la lista enlazada de los usuarios
 * @param nodo, el nodo cabecera desde el cual se comienza a mostrar informacion
 */
void printList(Node *nodo){
	while(nodo != NULL){
		printf("%s\n", (nodo->user).description);
		nodo = nodo->next;
	}
}

/**
 * Compara el username con la informacion del nodo y si esta coincide, devuelve el nodo
 * @param username, el username del usuario
 * @param nodo, el nodo cabecera
 * @return el nodo cuyo usuario coincide con la informacion de username
 */
User getNode(char *username, Node *nodo){
	while (nodo != NULL){
		if(strcmp(username, (nodo->user).username) == 0){
			return (nodo->user);
		}
		nodo = nodo->next;
	}
	return nullUser(); 
}


