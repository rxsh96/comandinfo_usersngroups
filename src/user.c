/**
 * user.c
 * Implementacion de los metodos para la estructura User.
 * @author Ricardo Serrano
 * @author Johnny Beltran
 */

/**
 *Includes 
 */
#include <user.h>

/**
 * Muestra por pantalla la informacion completa de un usuario
 * @param user, el user del que se quiere imprimir la informacion
 */
void toString(User user){
	printf("UID: %s\n", user.id);
	printf("Descripcion: %s\n", user.description);
	printf("Home: %s\n", user.home);
	printf("Shell: %s\n", user.shell);
}

/**
 * Inicializa el contenido de un usuario con cadenas vacias
 * @return user con sus elementos inicializados con cadena vacia
 */
User nullUser(){
	User user;
	user.id = "";
	user.username = "";
	user.description = "";
	user.home = "";
	user.shell = "";
	return user;
}
