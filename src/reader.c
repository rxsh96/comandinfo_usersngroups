/**
 * reader.c
 * Implementacion de los metodos para leer y guardar la informacion del archivo en estructuras.
 * @author Ricardo Serrano
 * @author Johnny Beltran
 */

/**
 *Includes 
 */
#include <reader.h>
#include <string.h>
#include <nodo.h>
#include <stdlib.h>
#include <flib.h>


/**
 * Lee el archivo filename, crea un usuario con cada linea del archivo y lo agrega a una lista enlazada
 * @param filename, la ruta absoluta del archivo
 * @param first, referencia al nodo cabecera
 * @return true si fue posible leer el archivo, false si el archivo no pudo ser leido
 */
bool loadUsers(char *filename, Node **first){
	FILE *file;
	if(validateFile(filename)){
		file = fopen(filename, "r");
		char buf[100];
		while((fgets(buf, sizeof(buf), file)) != NULL){
			User *user = malloc(sizeof(User));
			tokenizer(buf, user);
			Node *nodo = addNode(user, (*first));
			(*first) = nodo;
			
		}
		fclose(file);
		return true;
	}
	else{
		fprintf(stderr, "NO SE PUDO LEER EL ARCHIVO.\n");
		return false;
	}
	
}

/**
 * Lee el archivo filename, muestra por pantalla los usuarios que componen el grupo group
 * @param filename, la ruta absoluta del archivo
 * @param group, nombre del grupo
 * @param node, nodo cabedera
 * @return true si se encontro grupo, false en caso de que el grupo no exista o no tenga miembros asociados a el
 */
bool displayGroupUsers(char *filename, char *group, Node *node){
	FILE *file;
	if(validateFile(filename)){
		file = fopen(filename, "r");
		char buf[100];
		while((fgets(buf, sizeof(buf), file)) != NULL){
			char s[2] = ":";
			char comma[2] = ",";
			char *token = strtok(buf, s);
			if(strcmp(token, group) == 0){
				char *groupname = malloc(50*sizeof(char));			
				strcpy(groupname, token);
				token = strtok(NULL, s);
				token = strtok(NULL, s);
				token = strtok(NULL, s);
				char *username = groupname;
				User user = getNode(username, node);
				bool write = false;
				if(!(strcmp("", user.description) == 0)){ 
					printf("Miembros de %s\n", groupname);
					printf("Usuario: %s\n", username);
					printf("Descripcion: %s\n\n", user.description);
					write = true;
				}
				username = strtok(token, comma);
				if(strcmp("\n", username) == 0){
					if(!write) 
						printf("El grupo ingresado no tiene miembros!\n");				
					return false;
				}
				if(!write)
					printf("Miembros de %s\n", groupname);
				while(username != NULL){
					trim(username);
					user = getNode(username, node);
					if(strcmp(user.username, groupname)!=0){
						printf("Usuario: %s\n", user.username);
						printf("Descripcion: %s\n\n", user.description);
					}
					username = strtok(NULL, comma);
				}
				return true;
			}
		}
		fclose(file);
		fprintf(stderr, "El grupo ingresado no existe!\n");
		return false;
	}
	else{
		fprintf(stderr, "NO SE PUDO LEER EL ARCHIVO.\n");
		return false;
	}
}

/**
 * Asigna la informacion del archivo a cada miembro de la estructura User
 * @param buf, linea del archivo con la informacion del usuario
 * @param user, user que recibira dicha informacion
 */
void tokenizer(char *buf, User *user){
	char s[2] = ":";
	char *token = strsep(&buf, s);
	int index = 1;
	user->id = malloc(20*sizeof(char));
	user->username = malloc(50*sizeof(char));
	user->description = malloc(50*sizeof(char));
	user->home = malloc(50*sizeof(char));
	user->shell = malloc(50*sizeof(char));
	while(token != NULL){
		if(index == 1){
			strcpy(user->username, token);
		}
		if(index == 3){
			strcpy(user->id, token);
		}
		if(index == 5){
			saveDescriptionProperty(user, token);
		}
		if(index == 6){
			strcpy(user->home, token);
		}
		if(index == 7){
			strcpy(user->shell, token);
		}
		token = strsep(&buf, s);
		index++;
	}
}

