/**
 * main.c
 * @author Ricardo Serrano
 * @author Johnny Beltran
 */

/**
 *Includes 
 */
#include <user.h>
#include <nodo.h>
#include <reader.h>
#include <flib.h>

int main(int argc, char *argv[]){
	
	Node *nodo = NULL;
	User user;
	int option;
	
	bool loaded = loadUsers(USERFILENAME, &nodo);
	
	while((option = getopt(argc, argv, "gh")) != -1){
			switch(option){
				case 'g':
					if(argc == 3){
						displayGroupUsers(GROUPFILENAME, argv[2], nodo);
						return 0;
					}
					
				case 'h':
					showHelp();
					return 0;
				
				default:
					showHelp();
					return 0;
				
			}
		}
	
	
	if(argc == 2){
		if(loaded){
			user = getNode(argv[1], nodo);
			validateUser(user);
		}
		
	}
	else{
		fprintf(stderr, "Numero incorrecto de Argumentos!\n");
		showHelp();
		return -1;
	}
	
	return 0;
		
}



